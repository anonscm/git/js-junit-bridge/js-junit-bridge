/**
 * js-junit-bridge,
 * A JUnit Runner, that bridge the run and results of js-unit-test to junit.
 * Copyright (c) 2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.evolvis.java.jsjunitbridge;

import java.io.StringReader;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;

import javax.xml.xpath.XPathException;

import org.apache.xerces.xni.parser.XMLInputSource;
import org.cyberneko.html.parsers.DOMParser;
import org.evolvis.java.jsjunitbridge.utils.XPathUtil;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JSUnitRunner extends Runner {

	private Class<?> testClass;
	
	private Description suiteDescription;
	
	private NodeList webTestResults;
	
	/**
	 * Creates a JSUnitRunner to run {@code klass}
	 * 
	 * @throws InitializationError if the test class is malformed.
	 * @throws MalformedURLException 
	 * @throws XPathException 
	 */
	public JSUnitRunner(Class<?> testClass) throws InitializationError, MalformedURLException, XPathException {
		this.testClass = testClass;
		
		suiteDescription = Description.createSuiteDescription(testClass);

//	    System.setProperty("webdriver.firefox.profile", testClass.getName());
	    System.setProperty("webdriver.firefox.useExisting", "true");
	    System.setProperty("webdriver.reap_profile", "true");
		
        // The Firefox driver supports javascript 
        WebDriver driver = new FirefoxDriver();
//	    WebDriver driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        
        // Go to the Google Suggest home page
        driver.get("file:///home/trink/workspace/maven-qunit-test/src/main/resources/test/index.html");
		
        DOMParser parser = new DOMParser();
        try {
        	final XMLInputSource source = new XMLInputSource(null, "http://evolvis.org", null, new StringReader(driver.getPageSource()), "UTF-8");
    		parser.parse(source);
		}
        catch (Exception e) {
			e.printStackTrace();
			return;
		}
        finally {
			driver.close();
		}
		
        Document root = parser.getDocument();
        
        webTestResults = XPathUtil.getList("//*[contains(@id, 'test-output')]", root);
        
        for (int i = 0; i < webTestResults.getLength(); ++i) {
        	Node webTestResult = webTestResults.item(i);
			String moduleName = XPathUtil.getValue(".//*[contains(@class, 'module-name')]", webTestResult);
	        suiteDescription.addChild(Description.createSuiteDescription(moduleName, new Annotation[0]));
        }
     
	}
	
	@Override
	public Description getDescription() {
		return suiteDescription;
	}

	@Override
	public void run(RunNotifier notifier) {
        
        for (int i = 0; i < webTestResults.getLength(); ++i) {
        	
        	Node webTestResult = webTestResults.item(i);
        	
			String moduleName;
			String testName;
			try {
				moduleName = XPathUtil.getValue(".//*[contains(@class, 'module-name')]", webTestResult);
				testName = XPathUtil.getValue(".//*[contains(@class, 'test-name')]", webTestResult);
			} catch (XPathException e) {
				throw new RuntimeException(e);
			}
			 
			Description description = Description.createTestDescription(testClass, moduleName + "-" + testName);
			
			notifier.fireTestStarted(description);
			
			notifier.fireTestFinished(description);
        }
		
	}

}
