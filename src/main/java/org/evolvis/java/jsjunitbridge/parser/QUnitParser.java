/**
 * js-junit-bridge,
 * A JUnit Runner, that bridge the run and results of js-unit-test to junit.
 * Copyright (c) 2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.evolvis.java.jsjunitbridge.parser;

import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPathException;

import org.apache.xerces.xni.parser.XMLInputSource;
import org.cyberneko.html.parsers.DOMParser;
import org.evolvis.java.jsjunitbridge.WebResultParser;
import org.evolvis.java.jsjunitbridge.utils.XPathUtil;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Implements a WebResultParser for the QUnit testing framework.
 * 
 * @author Tino Rink <t.rink@tarent.de>
 */
public class QUnitParser implements WebResultParser {
	
	private Map<String, Description> moduleToSuiteDescription;
	
	private ArrayList<Description> testDescriptions;
	
	private Class<?> testClass;

	private Reader inputReader;
	
	private NodeList webTestResults;

	private Description suiteDescription;
	
	public void setTestClass(Class<?> testClass) {
		this.testClass = testClass;
	}
	
	public void setInput(Reader inputReader) {
		this.inputReader = inputReader;
	}

	private void parseInput() {
        final DOMParser parser = new DOMParser();
    	final XMLInputSource source = new XMLInputSource(null, "http://evolvis.org", null, inputReader, "UTF-8");
		
    	try {
			parser.parse(source);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
 
        final Document root = parser.getDocument();
        
		try {
			webTestResults = XPathUtil.getList("//*[contains(@id, 'test-output')]", root);
		} catch (XPathException e) {
			throw new RuntimeException(e);
		}
	}
	
	private NodeList getWebTestResults() {
		if (webTestResults == null) {
			parseInput();
		}
		return webTestResults;
	}
	
	private void createDescriptions() {
		
		suiteDescription = Description.createSuiteDescription(testClass);
		moduleToSuiteDescription = new HashMap<String, Description>();
		testDescriptions = new ArrayList<Description>();
		
		for (int i = 0; i < getWebTestResults().getLength(); ++i) {

			final Node webTestResult = getWebTestResults().item(i);
			
        	try {
        		
        		final String tmpModuleName = XPathUtil.getValue(".//*[@class='module-name']", webTestResult);
        		final String moduleName = (tmpModuleName != null && !"".equals(tmpModuleName)) ? tmpModuleName : "NoModule";
        		
        		if (! moduleToSuiteDescription.containsKey(moduleName)) {
    				final Description moduleDescription = Description.createSuiteDescription(moduleName);
    				suiteDescription.addChild(moduleDescription);
    				moduleToSuiteDescription.put(moduleName, moduleDescription);
        		}
        		
				final Description moduleDescription = moduleToSuiteDescription.get(moduleName);
	
				final String testName = XPathUtil.getValue(".//*[@class='test-name']", webTestResult);
				final Description testDescription = Description.createTestDescription(testClass, testName);
				
				moduleDescription.addChild(testDescription);
				
				testDescriptions.add(testDescription);
				
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
        }
	}
	
	private ArrayList<Description> getTestDescription() {
		if (testDescriptions == null) {		
			createDescriptions();
		}
		return testDescriptions;
	}
	
	public Description getDescription() {
		if (suiteDescription == null) {		
			createDescriptions();
		}
		return suiteDescription;
	}

	public void notifyJunit(RunNotifier notifier) {

		for (int i = 0; i < getWebTestResults().getLength(); ++i) {

			final Node webTestResult = getWebTestResults().item(i);
			final Description testDescription = getTestDescription().get(i);
        	
        	try {
				notifier.fireTestStarted(testDescription);
				
				final String testState = XPathUtil.getValue("./@class", webTestResult);
				
				if ("fail".equals(testState)) {
					final String errorMsg = XPathUtil.getValue(".//*[@class='fail']", webTestResult);
					notifier.fireTestFailure(new Failure(testDescription, new AssertionError(errorMsg)));
				}
				
				notifier.fireTestFinished(testDescription);
        	}
        	catch (Exception e) {
        		throw new RuntimeException(e);
        	}
        }
	}

}
