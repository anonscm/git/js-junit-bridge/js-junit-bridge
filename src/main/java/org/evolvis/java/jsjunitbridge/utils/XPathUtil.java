/**
 * js-junit-bridge,
 * A JUnit Runner, that bridge the run and results of js-unit-test to junit.
 * Copyright (c) 2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.java.jsjunitbridge.utils;

import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Utility-Class for working with and caching of XPath expressions.
 * The class is thread save through ThreadLocal objects.
 *
 * @author Tino Rink <t.rink@tarent.de>
 */
public class XPathUtil {

    private static final ThreadLocal<XPathUtil> instance = new ThreadLocal<XPathUtil>() {
        @Override
        protected XPathUtil initialValue() {
            return new XPathUtil();
        }
    };

    private final Map<String, XPathExpression> strExp2xpExp = new HashMap<String, XPathExpression>();

    private final XPathFactory xpathFactory = XPathFactory.newInstance();

    private final XPathExpression xpText;

    private XPathUtil() {
        try {
            xpText = getXPathExpression("text()");
        }
        catch (final Exception e) {
            throw new Error("XPathUtil() failed", e);
        }
    }

    private XPathExpression getXPathExpression(final String expression) throws XPathException {
        XPathExpression xpExp = strExp2xpExp.get(expression);
        if (null == xpExp) {
            xpExp = xpathFactory.newXPath().compile(expression);
            strExp2xpExp.put(expression, xpExp);
        }
        return xpExp;
    }

    /**
     * Return a nodeset for the given expression
     */
    public static <T extends Node> NodeList getList(final String expression, final T element) throws XPathException {
        final XPathExpression xpExp = instance.get().getXPathExpression(expression);
        return (NodeList) xpExp.evaluate(element, XPathConstants.NODESET);
    }

    /**
     * Return (first) element for the given expression
     */
    @SuppressWarnings("unchecked")
    public static <T extends Node> T getNode(final String expression, final T element) throws XPathException {
        final XPathExpression xpExp = instance.get().getXPathExpression(expression);
        return (T) xpExp.evaluate(element, XPathConstants.NODE);
    }

    /**
     * Return element value for the given expression
     */
    public static <T extends Node> String getValue(final String expression, final T element) throws XPathException {
        final XPathExpression xpExp = instance.get().getXPathExpression(expression);
        return (String) xpExp.evaluate(element, XPathConstants.STRING);
    }

    /**
     * Return value for the given element
     */
    public static String value(final Node element) throws XPathException {
        return instance.get().xpText.evaluate(element);
    }

    public static void setAttribute(String expression, String value, Node element) throws XPathException {
    	Node node = getNode(expression, element);

    	if (!(node instanceof Attr)) {
    		throw new XPathException("Expression does not denote an attribute node: "+expression);
    	}

    	((Attr) node).setValue(value);
    }

    public static void setContent(String expression, String value, Node element) throws XPathException {
    	Node node = getNode(expression, element);

    	node.setTextContent(value);
    }

    public static void removeNode(String expression, Node element) throws XPathException {
    	Node node = getNode(expression, element);
    	Node parent = node.getParentNode();

    	if (parent == null) {
    		throw new XPathException("Node cannot be removed if no parent is present");
    	}

    	parent.removeChild(node);
    }

    public static void replaceNode(String expression, Node replacement, Node element) throws XPathException {
    	Node node = getNode(expression, element);
    	Node parent = node.getParentNode();

    	if (parent == null) {
    		throw new XPathException("Node cannot be replaced if no parent is present");
    	}

    	parent.replaceChild(replacement, node);
    }
}