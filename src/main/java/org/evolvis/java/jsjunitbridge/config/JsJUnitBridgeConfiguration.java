/**
 * js-junit-bridge,
 * A JUnit Runner, that bridge the run and results of js-unit-test to junit.
 * Copyright (c) 2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.evolvis.java.jsjunitbridge.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.evolvis.java.jsjunitbridge.WebResultParser;

/**
 * @author Tino Rink <t.rink@tarent.de>
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface JsJUnitBridgeConfiguration {

	/**
	 * URI that should be tested
	 */
	String uri() default "http://localhost:8888";

	
	Class<? extends WebResultParser> resultParser();
	
	/**
	 * WebDriverConfigurations for each Browser, that should be used for local testing the uri
	 */
	WebDriverConfiguration[] localWebDriverConfigurations() default {};

	/**
	 * RemoteWebDriverConfiguration for each remote Browser, that should be used for remote testing the uri
	 */
	RemoteWebDriverConfiguration[] remoteWebDriverConfigurations() default {};
}
