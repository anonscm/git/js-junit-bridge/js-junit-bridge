/**
 * js-junit-bridge,
 * A JUnit Runner, that bridge the run and results of js-unit-test to junit.
 * Copyright (c) 2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.evolvis.java.jsjunitbridge;
import java.io.File;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.xpath.XPathException;

import org.evolvis.java.jsjunitbridge.config.JsJUnitBridgeConfiguration;
import org.evolvis.java.jsjunitbridge.config.RemoteWebDriverConfiguration;
import org.evolvis.java.jsjunitbridge.config.WebDriverConfiguration;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class JsJUnitBridgeRunner extends Runner {

	private WebResultParser webResultParser;
	
	/**
	 * Creates a JSUnitRunner to run {@code testClass}
	 */
	public JsJUnitBridgeRunner(Class<?> testClass) throws InitializationError, MalformedURLException, XPathException, InstantiationException, IllegalAccessException {

		List<WebDriver> localDrivers = getWebDrivers(testClass);
		List<WebDriver> remoteDrivers = getRemoteWebDrivers(testClass);
		
		List<WebDriver> drivers = new ArrayList<WebDriver>();
		drivers.addAll(localDrivers);
		drivers.addAll(remoteDrivers);
		
		if (drivers.isEmpty()) {
			throw new RuntimeException("At least one WebDriver (local or remote one) must be defined via @JsJUnitBridgeConfiguration");
		}
		
		URI testUri = null;
		try {
			final String testUriString = getTestUri(testClass);
			testUri = new URI(testUriString);
			
			final String scheme = testUri.getScheme();
			if ("file".equalsIgnoreCase(scheme)) {
				
				if (!remoteDrivers.isEmpty()) {
					throw new RuntimeException("Defined URI is a local filepath, so only local WebDriver could be defined in @JsJUnitBridgeConfiguration");
				}
				
				File testFile = new File(testUri.getPath());
				
				if (!testFile.exists()) {
					testFile = new File(System.getProperty("user.dir"), testUri.getPath());
				}

				if (!testFile.exists()) {
					throw new RuntimeException("Defined URI is a local filepath but doesn't exists, check defined uri in @JsJUnitBridgeConfiguration, filepath(s): "
							+ new LinkedHashSet<String>(Arrays.asList(testUri.getPath(), testFile.getPath())));
				}

				testUri = testFile.toURI();
			}
		}
		catch (URISyntaxException e) {
			throw new RuntimeException("Defined URI was invaild defined in @JsJUnitBridgeConfiguration, error: " + e.getMessage());
		}	
		
		//FIXME Currently only the first (local or remote) WebDriver runs
		final WebDriver driver = drivers.iterator().next();	
		
		driver.get(testUri.toString());
		
		webResultParser = getWebResultParser(testClass);
		
		webResultParser.setTestClass(testClass);
		webResultParser.setInput(new StringReader(driver.getPageSource()));

		driver.close();
	}
	
	WebResultParser getWebResultParser(Class<?> testClass) throws InstantiationException, IllegalAccessException {
		return testClass.getAnnotation(JsJUnitBridgeConfiguration.class).resultParser().newInstance();
	}
	
	List<WebDriver> getWebDrivers(Class<?> testClass) throws InstantiationException, IllegalAccessException {
		final List<WebDriver> webDrivers = new ArrayList<WebDriver>();
		for (WebDriverConfiguration driverConfigurations : testClass.getAnnotation(JsJUnitBridgeConfiguration.class).localWebDriverConfigurations()) {
			final WebDriver webDriver = driverConfigurations.driver().newInstance();
			webDrivers.add(webDriver);
		}
		return webDrivers; 
	}
	
	List<WebDriver> getRemoteWebDrivers(Class<?> testClass) throws InstantiationException, IllegalAccessException, MalformedURLException {
		final List<WebDriver> webDrivers = new ArrayList<WebDriver>();
		for (RemoteWebDriverConfiguration driverConfigurations : testClass.getAnnotation(JsJUnitBridgeConfiguration.class).remoteWebDriverConfigurations()) {
			final URL remoteServerAddress = new URL(driverConfigurations.remoteServer()); 
			final DesiredCapabilities desiredCapabilities = new DesiredCapabilities(
					driverConfigurations.browser(),
					driverConfigurations.version(),
					driverConfigurations.platform()
			);
			final WebDriver webDriver = new RemoteWebDriver(remoteServerAddress, desiredCapabilities);
			webDrivers.add(webDriver);
		}
		return webDrivers; 
	}
	
	String getTestUri(Class<?> testClass) {
		
		 String testUri = testClass.getAnnotation(JsJUnitBridgeConfiguration.class).uri();
		 
		 for (Entry<Object, Object> keyValue : System.getProperties().entrySet()) {
			 testUri = testUri.replaceAll("\\$\\{" + String.valueOf(keyValue.getKey()) + "\\}", String.valueOf(keyValue.getValue()));
		 }
		 
		 for (Entry<String, String> keyValue : System.getenv().entrySet()) {
			 testUri = testUri.replaceAll("\\$\\{" + String.valueOf(keyValue.getKey()) + "\\}", String.valueOf(keyValue.getValue()));
		 }
		 
		 return testUri; 
	}
	
	@Override
	public Description getDescription() {
		return webResultParser.getDescription();
	}

	@Override
	public void run(RunNotifier notifier) {
        webResultParser.notifyJunit(notifier);
	}

}
