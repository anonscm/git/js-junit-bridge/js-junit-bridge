/**
 * js-junit-bridge,
 * A JUnit Runner, that bridge the run and results of js-unit-test to junit.
 * Copyright (c) 2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.evolvis.java.jsjunitbridge;

import java.io.Reader;

import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;

public interface WebResultParser {

	/**
	 * Set the test class, needed for compatibility
	 */
	void setTestClass(Class<?> testClass);	
	
	/**
	 * Set the input source for the WebResultParser, this should be the html result
	 */
	void setInput(Reader inputReader);
	
	/**
	 * Creates a JUnit compatible Description of the parsed web tests
	 */
	Description getDescription();
	
	/**
	 * Notify JUnit about the results of the parsed web tests
	 */
	void notifyJunit(RunNotifier notifier);
}
